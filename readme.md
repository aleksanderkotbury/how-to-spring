**Spring Boot example with MongoDB**

To enable security uncomment dependency lines in build.gradle file,
then fill src/main/resources/application.yml with values from developers.facebook.com

In Valid OAuth redirect URIs paste `http://localhost:8080/login/oauth2/code/facebook`

**How to build?**
`./gradlew build` `gradlew.bat build`

**How to run?**
`./gradlew bootRun`
or fat jar (after build)
`build/libs/how-to-spring-1.0-SNAPSHOT.jar`
