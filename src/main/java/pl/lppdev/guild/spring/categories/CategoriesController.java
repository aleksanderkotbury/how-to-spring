package pl.lppdev.guild.spring.categories;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.lppdev.guild.spring.products.Product;
import pl.lppdev.guild.spring.products.ProductsRepository;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoriesController {

    private final ProductsRepository productsRepository;

    public CategoriesController(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @GetMapping("/{categoryName}/products")
    public List<Product> getCategoryProducts(@PathVariable String categoryName) {
        return productsRepository.findByCategoriesContaining(categoryName);
    }

}
