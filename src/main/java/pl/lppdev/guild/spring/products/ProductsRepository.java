package pl.lppdev.guild.spring.products;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProductsRepository extends MongoRepository<Product, String> {

    List<Product> findByCategoriesContaining(String category);
}
