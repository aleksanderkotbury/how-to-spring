package pl.lppdev.guild.spring.products;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/products")
public class ProductsControllers {

    private final ProductsRepository productsRepository;

    public ProductsControllers(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @GetMapping
    @Cacheable("products")
    public ResponseEntity<Page<Product>> getProducts(Pageable pageable) {
        Page<Product> products = productsRepository.findAll(pageable);
        return ResponseEntity.ok()
                .header("X-Last-Modified", LocalDateTime.now().toString())
                .body(products);
    }

    @GetMapping(path = "/{id}")
    public Product getById(@PathVariable("id") String productId) {
        return productsRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Product create(@RequestBody Product product) {
        return productsRepository.insert(product);
    }
}
