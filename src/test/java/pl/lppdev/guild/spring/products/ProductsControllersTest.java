package pl.lppdev.guild.spring.products;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.lppdev.guild.spring.Application;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = Application.class, properties = "spring.data.mongodb.port: 0")
public class ProductsControllersTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductsRepository productsRepository;

    @Before
    public void setUp() throws Exception {
        productsRepository.deleteAll();
    }

    @Test
    @WithMockUser
    public void shouldReturnAllSimulations() throws Exception {
        // given
        String[] shirts = {"t-shirt", "shirt 1", "some shirt", "blue shirt", "black shirt"};
        createShirts(shirts);

        // when
        mockMvc.perform(get("/products"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("content.*", hasSize(shirts.length)))
                .andExpect(MockMvcResultMatchers.jsonPath("content.*.name", contains(shirts)));

    }

    @Test
    @WithMockUser
    public void shouldReturnProductById() throws Exception {
        // given
        Product product = new Product(null, "jeans", "awesome jeans", Collections.emptyList());
        Product savedProduct = productsRepository.save(product);

        // when
        mockMvc.perform(get("/products/{id}", savedProduct.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("name", equalTo("jeans")));
    }

    @Test
    @WithMockUser
    public void shouldReturnNotFoundException_whenProductDoesNotExist() throws Exception {
        // when
        mockMvc.perform(get("/products/not_existing_id"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    private void createShirts(String... names) {
        List<Product> shirtCollection = Stream.of(names)
                .map(name -> new Product(null, name, "desc " + name, Collections.singletonList("shirt")))
                .collect(Collectors.toList());

        productsRepository.saveAll(shirtCollection);
    }
}