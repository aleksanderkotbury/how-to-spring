package pl.lppdev.guild.spring.categories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.lppdev.guild.spring.Application;
import pl.lppdev.guild.spring.products.Product;
import pl.lppdev.guild.spring.products.ProductsRepository;

import java.util.Arrays;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = Application.class, properties = "spring.data.mongodb.port: 0")
public class CategoriesControllerTest {

    @Autowired
    private ProductsRepository productsRepository;
    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        productsRepository.deleteAll();
    }

    @Test
    @WithMockUser
    public void shouldReturnOnlyProductsFromGivenCategory() throws Exception {
        // given
        createProduct("shirt 1", "men", "shirt");
        createProduct("shirt 2", "men", "shirt");
        createProduct("shirt 3", "men", "polo");
        createProduct("shirt 4", "women", "shirt");

        // when
        mockMvc.perform(get("/categories/{id}/products", "men"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.*.name", contains("shirt 1", "shirt 2", "shirt 3")));
    }

    private void createProduct(String name, String... category) {
        productsRepository.save(new Product(null, name, "", Arrays.asList(category)));
    }

}